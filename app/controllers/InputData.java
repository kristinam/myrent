package controllers;

import java.util.Date;

import models.Residence;
import models.User;
import play.Logger;
import play.mvc.Controller;
import org.json.simple.JSONObject;

public class InputData extends Controller
{

  /**
   * Render data input view (logged-in users only).
   */
  public static void index()
  {
    if (session.get("logged_status") != null && session.get("logged_status").equals("logged_in"))
    {
      render();
    }
    else
    {
      Accounts.login();
    }
  }

  /**
   * Update models with residential data
   */
  public static void dataCapture(Residence residence)
  {
    User user = Accounts.getCurrentUser();
    residence.addUser(user);   
    residence.dateRegistered = new Date();
    residence.save();

    Logger.info("Residence data received and saved");
    Logger.info("Residence type: " + residence.residenceType);
    Logger.info("Rented? " + residence.rented);

    //index();
    JSONObject obj = new JSONObject();
    String value = "Congratulations. You have successfully registered your " + residence.residenceType + ".";
    obj.put("inputdata", value);
    renderJSON(obj);
  }

}
