
package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.db.jpa.Model;
import utils.LatLng;

@Entity
public class Residence extends Model
{
  @ManyToOne
  public User user;

  // residence location in latitude longitude
  // example: "52.253456, -7.18716"
  public String geolocation; 

  // date residence registered 
  public Date dateRegistered;

  // rented == 'yes' represents residence rented, 
  // rented == 'no'  represents building vacant
  public String rented;

  // rent per calendar month
  public int rent;

  public int numberBedrooms;

  // Apartment | Flat, Studio or House
  public String residenceType;
  

  public void addUser(User user)
  {
    this.user = user;
    this.save();
  }
  public LatLng getGeolocation()
  {
    return LatLng.toLatLng(this.geolocation);
  }
  
}